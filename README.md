# Nom du Projet Angular

Ce projet est une application web développée avec Angular. Elle affiche une liste de livres de Stephen King, permettant aux utilisateurs de voir les couvertures des livres et d'accéder à des détails supplémentaires sur chaque livre.

## Fonctionnalités Principales

- Affiche une liste de livres de Stephen King.
- Permet aux utilisateurs de filtrer les livres par ordre alphabétique.
- Offre la possibilité de voir des détails supplémentaires sur chaque livre.

## Technologies Utilisées

- **Angular**: Un framework de développement pour la création d'applications web single-page.
- **TypeScript**: Un sur-ensemble de JavaScript qui ajoute des types statiques.
- **Netlify**: Une plateforme de déploiement et d'hébergement pour les applications web front-end.

## Commencer

Pour commencer à travailler avec ce projet, suivez ces étapes :

### Prérequis

Assurez-vous d'avoir installé Node.js et le gestionnaire de paquets npm sur votre système. Angular CLI doit également être installé pour exécuter et construire le projet.

### Installation

1. Clonez ce dépôt sur votre machine locale :

```bash
git clone https://gitlab.com/flojarno/stephen-king-library.git
```
Naviguez dans le dossier du projet et installez les dépendances :
```bash
cd nom-du-projet-angular
npm install
```
Pour lancer l'application en mode développement, exécutez :
```bash
ng serve
```
Ouvrez http://localhost:4200 pour voir l'application dans le navigateur.