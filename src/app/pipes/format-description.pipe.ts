import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatDescription',
  standalone: true
})
export class FormatDescriptionPipe implements PipeTransform {

  transform(description: string | {value: string}): string {
    if (typeof description === 'string') {
      return description
    } else {
      return description.value;
    }
  }
}
