import { Pipe, PipeTransform } from '@angular/core';
import { Book } from '../models/book';

@Pipe({
  name: 'bookFilter',
  standalone: true
})
export class BookFilterPipe implements PipeTransform {

  transform(bookList: Book[], activeFilter: boolean): Book[] {
    if (activeFilter) {
      return bookList.slice().sort((a, b) => {
        const titleA = a.title?.toUpperCase(); 
        const titleB = b.title?.toUpperCase();
        if (titleA < titleB) {
          return -1;
        }
        if (titleA > titleB) {
          return 1;
        }
        return 0; // Si les titres sont identiques
      });
    }
    return bookList;
  }
}
