import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'coverDisplay',
  standalone: true
})
export class CoverDisplayPipe implements PipeTransform {

  transform(coverId: number): string {
    if (coverId && coverId !== -1) {
      return `https://covers.openlibrary.org/b/id/${coverId}-M.jpg`;
    } else {
      return 'https://placehold.co/180x273';
    }
  }
}
