export interface Book {
    title: string;
    covers: number[];
    key: string;
    first_publish_date: Date;
    description: string | { value: string; };
    subjects: string[];
    revision: number;
}