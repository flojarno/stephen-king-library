import { Book } from "./book";

export interface ApiResponse {
    entries: Book[];
}
