import { Component } from '@angular/core';
import { BookService } from '../book.service';
import { Book } from '../models/book';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from '../button/button.component';
import { Router, RouterLink } from '@angular/router';
import { ApiResponse } from '../models/api-response';
import { BookFilterPipe } from "../pipes/book-filter.pipe";
import { CoverDisplayPipe } from '../pipes/cover-display.pipe';

@Component({
    selector: 'app-book-list',
    standalone: true,
    templateUrl: './book-list.component.html',
    styleUrl: './book-list.component.css',
    imports: [
        CommonModule,
        ButtonComponent,
        RouterLink,
        BookFilterPipe,
        CoverDisplayPipe
    ]
})
export class BookListComponent {

  constructor(private bookService: BookService, private router: Router){}
  bookList: Book[] = [];
  selectedFilter: boolean = false;

  ngOnInit(){

    this.bookService.getBooks().subscribe(
      (data: ApiResponse) => {
        console.log('Books fetched:', data.entries);
        this.bookList = data.entries;
      },
      (error) => {
        console.error('Error fetching books:', error);
    })    
  } 

  viewBookDetails(workId: string) {
    this.router.navigate(['/books', workId]);
  }

  toggleFilter(){
    this.selectedFilter = !this.selectedFilter;
  }
}

