import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Book } from './models/book';
import { Observable } from 'rxjs';
import { ApiResponse } from './models/api-response';

@Injectable({
  providedIn: 'root',
})
export class BookService {

  constructor(private http: HttpClient) { }

  getBooks(): Observable<ApiResponse> {
    console.log("appel API pour la liste")
    return this.http.get<ApiResponse>('https://openlibrary.org/authors/OL2162284A/works.json?limit=306');
  }

  getBookDetails(workId: string): Observable<Book>{
    console.log("appel API pour les détails d'un livre depuis son work ID")
    const url = `https://openlibrary.org${workId}.json`;
    return this.http.get<Book>(url);
  }
}

 