import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookService } from '../book.service';
import { Book } from '../models/book';
import { CommonModule } from '@angular/common';
import { FormatDescriptionPipe } from "../pipes/format-description.pipe";
import { CoverDisplayPipe } from '../pipes/cover-display.pipe';

@Component({
    selector: 'app-book-details',
    standalone: true,
    templateUrl: './book-details.component.html',
    styleUrl: './book-details.component.css',
    imports: [
      CommonModule, 
      FormatDescriptionPipe,
      CoverDisplayPipe
  ]
})
export class BookDetailsComponent {

  bookID: string = '';
  bookDetails!: Book;
typeOf: any;
  
  constructor(private route: ActivatedRoute, private bookService: BookService) {}

  ngOnInit() {
    this.bookID = this.route.snapshot.paramMap.get('workId') || '';
    if (this.bookID) {
      this.bookService.getBookDetails(this.bookID).subscribe((details: Book) => {
        console.log("DETAILS", details)
        this.bookDetails = details;
      });
    }
  }
}

// 1.
// ActivatedRoute => pour accéder aux paramètres de la route
// ActivatedRoute.snapshot.paramMap.get('workId') permet de récupérer la valeur de workId passée dans l'URL
// Cette valeur est stockée dans la propriété bookID du composant

// 2.
// On passe le bookId en parametre de la méthode getBookDetails() 
// pour construire l'url qui permet de récupérer les infos d'un livre 
// ex:  https://openlibrary.org${workId}.json ou workId = /works/123456789